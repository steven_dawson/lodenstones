# LodenStones

The game contains the rules and information neccissary to play.

This repository is meant to show my progress as I add features and squash bugs as well as serving as a backup incase I do something horribly wrong.

The first commit is a sort of Proof of Concept. the game is playable, a few things aren't exactly how I want and the code isn't always pretty, but it works. 
I'm not to worried about this, however, as the rules of the game will likely change as I get feedback about what is possible and what should be impossible.

Future commits will include updates to the rules, improved animations and graphics, and even network connectivity. 

One of the end goals of this project was to learn how to use Unity's Servers to create an online multiplayer game. 